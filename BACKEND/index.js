const bodyParser = require('body-parser')
const express = require('express')
const cors = require('cors')
const app = express()
const PORT = 9000

//konfigurasi database
global.config = require('./config/dbconfig')

var corsOption = {
    origin: 'http://localhost:9500'
}

app.use(cors(corsOption))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// simple route
app.get('/welcome', (req, res) => {
    res.json({ message: 'Welcome to Aplication' })
})

require('./service/karyawanService')(app, global.config.pool)
require('./service/positionService')(app, global.config.pool)

app.listen(PORT, () => {
    console.log('Server is Running on port ' + PORT)
})