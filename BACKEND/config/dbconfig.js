const Pool = require('pg').Pool

const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'karyawanDB',
    password: '1234567890',
    port: 8080
})

module.exports = {
    pool
}