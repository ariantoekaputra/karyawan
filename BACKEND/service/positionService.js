module.exports = exports = (app, pool) => {
    app.get('/api/position', (req, res) => {
        const query = `select * from t1_position`
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            }
            else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    })


}