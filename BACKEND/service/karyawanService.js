module.exports = exports = (app, pool) => {
    app.get('/api/karyawan', (req, res) => {
        const query = `select e.id as No, e.name, birth_date, p.name as jabatan, id_number, gender,
        CASE WHEN gender = 1 then 'Pria'
        else 'Wanita' end AS jenis_kelamin
        from t2_employee as e
        join t1_position as p
        on e.position_id=p.id
        where e.is_delete= 0`
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            }
            else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
    })

    app.post('/api/addkaryawan', (req, res) => {
        const { name,birth_date,position_id,id_number,gender } = req.body
        const query = `INSERT INTO t2_employee(
            name, birth_date,position_id,id_number,gender,is_delete)
            VALUES ('${name}', '${birth_date}', ${position_id}, ${id_number}, ${gender},0) `

        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: `Data ${name} save`
                })
            }
        })
    })

    app.put('/api/updatekaryawan/:id', (req, res) => {
        const id = req.params.id
        const {name,birth_date,position_id,id_number,gender } = req.body
        const query = `UPDATE t2_employee SET name='${name}', birth_date= '${birth_date}',
        position_id = ${position_id}, id_number = ${id_number}, gender=${gender}
        WHERE id = ${id}`
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: `Data ${name} as been updated`
                })
            }
        })
    })
    app.put('/api/deletekaryawan/:id', (req, res) => {
        const id = req.params.id
        const { name } = req.body
        const query = `UPDATE t2_employee SET is_delete=1
        WHERE id = ${id}`
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            } else {
                res.send(200, {
                    success: true,
                    result: `Data ${name} as been updated`
                })
            }
        })
    })

    app.get('/api/getkaryawan/:id', (req, res) => {
        const id = req.params.id
        const query = `select e.id as No, e.name, birth_date, p.name as jabatan, id_number, gender,
        CASE WHEN gender = 1 then 'Pria'
        else 'Wanita' end AS jenis_kelamin
        from t2_employee as e
        join t1_position as p
        on e.position_id=p.id WHERE e.id = ${id}`
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    result: error
                })
            }
            else {
                res.send(200, {
                    success: true,
                    result: result.rows
                })
            }
        })
        console.log(query)
    })

}