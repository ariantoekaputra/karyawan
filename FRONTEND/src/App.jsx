import React from 'react'
import KaryawanIndex from './karyawan'
import Header from './layout/header'
import Sidebar from './layout/sidebar'
import Footer from './layout/footer'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

class App extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <div class="wrapper">

                    <Header />
                    <Sidebar />

                    <div class="content-wrapper">
                        <section class="content">
                            <Switch>
                                
                                <Route exact path='/karyawan' component={KaryawanIndex}>
                                    <KaryawanIndex />
                                </Route>
                            </Switch>
                        </section>
                    </div>
                    <Footer />
                </div>
            </BrowserRouter>
        )
    }
}
export default App