import axios from 'axios'
import { config } from '../config/config'

const karyawanService = {
    getAll: () => {
        //http://localhost:9000/api/karyawan
        const result = axios.get(config.apiUrl + '/karyawan')
            .then(respon => {
                return {
                    success: respon.data.success,
                    result: respon.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    error: error
                }
            })
        return result
    },


    addData: (data) => {
        const result = axios.post(config.apiUrl + '/addkaryawan', data)
            .then(respon => {
                return {
                    success: respon.data.success,
                    result: respon.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    reault: error
                }
            })
        return result
    },
    
    editData: (data) => {
        console.log(data)
        const result = axios.put(config.apiUrl + '/updatekaryawan/' + data.id, data)

            .then(respon => {
                return {
                    success: respon.data.success,
                    result: respon.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            })
        return result
    },
    deleteData: (data) => {
        const result = axios.put(config.apiUrl + '/deletekaryawan/' + data.id, data)
            .then(respon => {
                return {
                    success: respon.data.success,
                    result: respon.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            })
        return result
    },
    getById: (id) => {
        const result = axios.get(config.apiUrl + '/getkaryawan/' + id)
            .then(respon => {
                return {
                    success: respon.data.success,
                    result: respon.data.result[0]
                }
            })
            .catch(error => {
                return {
                    success: false,
                    result: error
                }
            })
        return result
    }
}
export default karyawanService