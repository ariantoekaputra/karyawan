import axios from 'axios'
import { config } from '../config/config'

const positionService = {
    getAll: () => {
        //http://localhost:9000/api/position
        const result = axios.get(config.apiUrl + '/position')
            .then(respon => {
                return {
                    success: respon.data.success,
                    result: respon.data.result
                }
            })
            .catch(error => {
                return {
                    success: false,
                    error: error
                }
            })
        return result
    }
}
export default positionService