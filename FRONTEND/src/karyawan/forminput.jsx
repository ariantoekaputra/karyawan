import React from 'react'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Select from 'react-select'
import Calendar from 'react-input-calendar'

export default class FormInput extends React.Component {

    constructor() {
        super()
        this.state = {
            selectedOption: {}
        }
    }

    render() {
        const { changeHandler, checkedHandler, saveHandler,
            KaryawanModel, showModal, closeHandler, mode, sureDeleteHandler,
            errors, positionList, selectedHandler } = this.props
        let options = positionList.map(function (item) {
            return { value: item.id, label: item.name }
        })
        return (
            <div>
                 {JSON.stringify(KaryawanModel)}

                <Modal show={showModal} onHide={closeHandler} backdrop='static' style={{ opacity: 1 }} dialogClassName="my-modal">
                    <Modal.Header closeButton>
                        <Modal.Title><h2>Tambah Karyawan</h2></Modal.Title>
                    </Modal.Header>
                    {mode === 'delete' ?
                        <Modal.Body>
                            <div className='modal-body'>
                                <p>Are You Sure delete data {KaryawanModel.name}?</p>
                            </div>
                        </Modal.Body>
                        :
                        <Modal.Body>

                            <label class="control-label col-sm-2" for="name">Name</label>
                            <input type="name" class="form-control" id="name" placeholder="Nama Sesuai Dengan KTP" name="name"
                                value={KaryawanModel.name} onChange={changeHandler('name')} ></input>
                            <span style={{ color: "red" }}>{errors["name"]}</span><br />

                            <label class="control-label col-sm-2" for="birth_date">Tanggal Lahir</label>
                            <input type="date" id="birthday" name="birthday"/><br></br>

                            <label class="control-label col-sm-2" for="Jabatan">Jabatan</label>
                            <Select name="category_id"
                                value={mode == 'create' ? this.state.value : {
                                    label: KaryawanModel.jabatan,
                                    value: KaryawanModel.id
                                }}
                                onChange={selectedHandler}
                                options={options} />

                            <label class="control-label col-sm-2" for="NIP">NIP</label>
                            <input type="nip" class="form-control" id="name" placeholder="Nama Sesuai Dengan KTP" name="name"
                                value={KaryawanModel.name} onChange={changeHandler('name')} ></input>

                            <label class="control-label col-sm-2" for="stock">Jenis Kelamin</label>
                            <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gridRadios" id="1" value={KaryawanModel.gender} checked />
                                    <label class="form-check-label" for="gridRadios1">Pria </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gridRadios" id="2" value={KaryawanModel.gender} />
                                    <label class="form-check-label" for="gridRadios2"> Wanita</label>
                                </div>
                            
                            <span style={{ color: "red" }}>{errors["stock"]}</span><br /></Modal.Body>
                    }
                    {mode === 'delete' ?
                        < Modal.Footer >
                            <Button variant="secondary" onClick={() => sureDeleteHandler(KaryawanModel)}>Sure</Button>
                            <Button variant="primary" onClick={closeHandler}>close</Button>
                        </Modal.Footer>
                        :
                        <Modal.Footer>
                            <Button variant="secondary" onClick={closeHandler}>Close</Button>
                            <Button variant="primary" onClick={saveHandler} >Save Changes</Button>
                        </Modal.Footer>
                    }
                </Modal>
            </div >


        )
    }
}