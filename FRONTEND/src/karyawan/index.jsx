import React from 'react'
import './index.css'
import KaryawanService from '../service/karyawanService'
import FormInput from './forminput'
import PositionService from '../service/positionService'

export default class Index extends React.Component {
    KaryawanModel = {
        id: 0,
        name: '',
        birth_date: '',
        position_id: 0,
        id_number: 0,
        gender:0,
        jenis_kelamin: '',
        jabatan: '',
        is_delete: 0



    }
    constructor() {
        super()
        this.state = {

            ListKaryawan: [],
            KaryawanModel: this.KaryawanModel,
            mode: '',
            showModal: false,
            errors: {},
            option: '',
            positionList: [],
            

        }
    }
    componentDidMount() {
        this.loadList()
    }
    loadList = async () => {
        this.getPositionList()
        const respon = await KaryawanService.getAll()
        console.log(respon)
        if (respon.success) {
            this.setState({
                ListKaryawan: respon.result,
            })
        }
    }
    
    selectedHandler = (selectedOption) => {
        this.setState({
            KaryawanModel: {
                ...this.state.KaryawanModel,
                position_id: selectedOption.value,
                jabatan: selectedOption.label
            }
        })
    }
    changeHandler = name => ({ target: { value } }) => {
        this.setState({
            KaryawanModel: {
                ...this.state.KaryawanModel,
                [name]: value
            }

        })
    }
    getPositionList = async () => {
        const respon = await PositionService.getAll()
        this.setState({
            positionList: respon.result
        })
        
    }
    createHandler = () => {
        this.getPositionList()
        this.setState({
            KaryawanModel: this.KaryawanModel,
            mode: 'create',
            showModal: true
        })
    }
    editHandler = async (id) => {
        const respon = await KaryawanService.getById(id)
        console.log(respon)
        this.getPositionList()
        if (respon.success) {
            this.setState({
                KaryawanModel: respon.result,
                mode: 'edit',
                showModal: true
            })
        }
    }
    checkedHandler = name => ({ target: { checked } }) => {
        this.setState({
            KaryawanModel: {
                ...this.state.KaryawanModel,
                [name]: checked
            }

        })
    }
    saveHandler = async () => {
        const { KaryawanModel, mode } = this.state

        if (mode === 'edit') {
            if (this.validationHandler()) {
                const respon = await KaryawanService.editData(KaryawanModel)
                if (respon.success) {
                    alert('Success ' + respon.result)
                    this.loadList()
                    console.log(respon.success)
                }
                else {
                    alert('Error ' + respon.result)
                    console.log(respon.result)
                }
            }
        }
        else {
            if (this.validationHandler()) {
                const respon = await KaryawanService.addData(KaryawanModel)

                if (respon.success) {
                    alert('Success ' + respon.result)
                    this.loadList()
                    this.setState({
                        KaryawanModel: this.KaryawanModel,
                        showModal: false
                    })
                }
                else {
                    alert('Error ' + respon.result)
                }
            }
        }
    }
    validationHandler() {
        const { KaryawanModel } = this.state
        let errors = {}
        let formField = true


        if (!KaryawanModel["name"]) {
            formField = false
            errors["name"] = "Isi Nama Produk"
        }
        if (!KaryawanModel["description"]) {
            formField = false
            errors["description"] = "Isi deskripsi Product"
        }
        if (!KaryawanModel["price"]) {
            formField = false
            errors["price"] = "Lu Jualan nggak ada Harga apa"
        }
        if (!KaryawanModel["stock"]) {
            formField = false
            errors["stock"] = "Buat apa Nambahin produk kalau kagak ada stock"
        }
        if (!KaryawanModel["category_id"]) {
            formField = false
            errors["category_id"] = "Category apa"
        }
        this.setState({ errors: errors })
        return formField
    }
    deleteHandler = (data) => {
        this.setState({
            showModal: true,
            mode: 'delete',
            KaryawanModel: data
        })
    }
    sureDeleteHandler = async (data) => {
        const respon = await KaryawanService.deleteData(data)
        if (respon.success) {
            alert('Success ' + respon.result)
            this.loadList()
        }
        else {
            alert('Error ' + respon.result)
            console.log(respon.result)
        }
        this.setState({
            showModal: false,
            mode: '',
            KaryawanModel: this.KaryawanModel
        })
    }
    closeHandler = () => {
        this.setState({
            KaryawanModel: this.KaryawanModel,
            showModal: false
        })
    }
    changeHandler = name => ({ target: { value } }) => {
        this.setState({
            KaryawanModel: {
                ...this.state.KaryawanModel,
                [name]: value
            }

        })
    }
    
    render() {
        const { ListKaryawan, KaryawanModel, showModal, mode, errors, positionList } = this.state
        return (
            <div >
                {JSON.stringify(positionList)}
                <div>
                    <center><h1>Master Karyawan</h1></center>
                </div>
                
                <FormInput changeHandler={this.changeHandler}
                    checkedHandler={this.checkedHandler}
                    saveHandler={this.saveHandler}
                    KaryawanModel={KaryawanModel}
                    showModal={showModal}
                    closeHandler={this.closeHandler}
                    mode={mode}
                    sureDeleteHandler={this.sureDeleteHandler}
                    errors={errors}
                    positionList={positionList}
                    selectedHandler={this.selectedHandler}
                />
                <nav class="navbar navbar-light bg-light justify-content-between">
                    <div class="card card-success collapsed-card">
                            <div class="input-group input-group-sm">
                                    <button class="btn btn-primary" type="button" onClick={this.createHandler}>
                                        <i class="fas fa-plus">TAMBAH</i>
                                    </button>
                            </div>
                    </div>
                </nav>
                <table>
                    <thead>
                        <tr>
                            <th>NAME</th>
                            <th>TANGGAL LAHIR</th>
                            <th>JABATAN</th>
                            <th>NIP</th>
                            <th>JENIS KELAMIN</th>
                            <td>AKSI</td>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            ListKaryawan.map(data => {
                                return (
                                    <tr key={data.id}>
                                        <td>{data.name}</td>
                                        <td>{data.birth_date}</td>
                                        <td>{data.jabatan}</td>
                                        <td>{data.id_number}</td>
                                        <td>{data.jenis_kelamin}</td>
                                        <td>
                                            <button width="10px" type="button" class="btn btn-success btn-lg" onClick={() => this.editHandler(data.id)}>Edit</button>
                                            <button type="button" class="btn btn-danger btn-lg" onClick={() => this.deleteHandler(data)} >Delete</button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div >
        )
    }
}