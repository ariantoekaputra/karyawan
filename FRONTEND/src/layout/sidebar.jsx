import React from 'react'

class Sidebar extends React.Component {
    render() {
        return (
            <div>
                <aside class="main-sidebar sidebar-dark-primary elevation-4">
                    <div class="sidebar">
                        <nav class="mt-2">
                            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                                <li class="nav-item has-treeview menu-open">
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <a href="/karyawan" class="nav-link">
                                                <i class="fa fa-database nav-icon"></i>
                                                <p>Karyawan</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </aside>
            </div>
        )
    }
}
export default Sidebar