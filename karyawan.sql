CREATE TABLE t1_position
(
id bigint GENERATED ALWAYS AS IDENTITY not null,
code varchar(50) not null,
name varchar(100) not null,
is_delete int not null
)

CREATE TABLE t2_employee
(
id bigint GENERATED ALWAYS AS IDENTITY not null,
name varchar(100) not null,
birth_date date not null,
position_id int not null,
id_number int not null,
gender int not null,
is_delete int not null
)
select *from t1_position
insert into t1_position(code,name,is_delete)
values
('SA','System Analyst',0),
('BPS','BPS',0),
('PRG','Programmer',0),
('TEST','Tester',0),
('HELP','Helpdesk',0)

select *from t2_employee
insert into t2_employee(name,birth_date,position_id,id_number,gender,is_delete)
values
('Yogi Lestari', '1990-02-14',5,14021990,1,0),
('Anggi Setiawan', '1991-05-10',2,10051991,1,0),
('Rosiana', '1993-04-20',4,20041993,2,0),
('Yudi Ismiaji', '1994-11-14',3,11011994,1,0)

select e.id as No, e.name, birth_date, p.name as jabatan, id_number, gender,
CASE WHEN gender = 1 then 'Pria'
else 'Wanita' end AS jenis_kelamin
from t2_employee as e
join t1_position as p
on e.position_id=p.id
where e.is_delete= 0

UPDATE t2_employee SET is_delete=1
WHERE id = 6

INSERT INTO t2_employee(
            name, birth_date,position_id,id_number,gender,is_delete)
            VALUES ('Arianto','1994-03-24', 3, 24031994, 1,0)

UPDATE t2_employee SET is_delete=1

select * from t1_position

select e.id as No, e.name, birth_date, p.name as jabatan, id_number, gender,
        CASE WHEN gender = 1 then 'Pria'
        else 'Wanita' end AS jenis_kelamin
        from t2_employee as e
        join t1_position as p
        on e.position_id=p.id WHERE e.id =1
